
% Test for colored noise derived from a real recording as in the BCI Competition IV review paper

clear;
close all;

% Use a real recording to estimate the spectral characteristics
freqLow = 0.5;
freqHigh = 60;

dat=load('E:/jl/datasets/motor-imagery-rennes-jussi-gtec2008-20160104/motor-imagery-csp-4-online-[2016.01.04-15.18.47].mat');
%dat=load('C:/jl/datasets/motor-imagery-rennes-jussi-gtec2008-20160104/motor-imagery-csp-4-online-[2016.01.04-15.23.42].mat');
%dat=load('C:/jl/datasets/motor-imagery-rennes-jussi-gtec2008-20160104/motor-imagery-csp-4-online-[2016.01.04-15.29.04].mat');
dat.X=dat.samples; dat.samples=[];
dat.X = filter_bandpass(dat.X, dat.samplingFreq, freqLow, freqHigh, 4);
dat.id='motorimagery';
%fprintf(1, 'Estimating cross spectrum...\n');
noiseModel = noisemodel_cross_spectral(...
	'source',dat.X, 'samplingFreq', dat.samplingFreq, 'window', dat.samplingFreq);

% visualize_dataset(dat, 'title', 'Source');

% Background noise
Xnoise = noiseModel.generate('nSamples',size(dat.X,1));

% See if the spectral characteristics of the generated data produce the same spectral model
noiseModel2 = noisemodel_cross_spectral(...
	'source', Xnoise, 'samplingFreq', dat.samplingFreq);

freqs = noiseModel.model.frequencies;
idxs = (freqs>=freqLow) & (freqs<=freqHigh);

spect = abs(noiseModel.model.crossSpectrum(:,:,idxs));
spect2 = abs(noiseModel2.model.crossSpectrum(:,:,idxs));

diagIdx = logical(repmat(eye(size(spect,1),size(spect,2)), [1 1 size(spect,3)]));

sdiag = reshape(spect(diagIdx),[size(spect,1),size(spect,3)]);
s2diag = reshape(spect2(diagIdx),[size(spect2,1),size(spect2,3)]);

figure(); clf; hold on;
cm = parula(size(sdiag,1));
for i=1:size(sdiag,1)
	plot(freqs(idxs), 10*log10(sdiag(i,:)), 'Color', cm(i,:));
	plot(freqs(idxs), 10*log10(s2diag(i,:)),'--','Color', cm(i,:));
end
grid;
title('Electrode spectra');

error = mean(abs( (sdiag(:)-s2diag(:))./sdiag(:)));
fprintf(1,'Relative error for spectra %f%%\n', 100*error);

crossError = mean(abs( (spect(:)-spect2(:))./spect2(:)));
fprintf(1,'Relative error for cross spectra %f%%\n', 100*crossError);

if(0)
	for i=1:size(noiseModel.model.crossSpectrum,3)
		subplot(1,2,1);
		imagesc(abs(squeeze(noiseModel.model.crossSpectrum(:,:,i))));
		subplot(1,2,2);
		imagesc(abs(squeeze(noiseModel2.model.crossSpectrum(:,:,i))));
		title(i);
		pause(0.1); 
	end
end

% cross-spectrum correlation
r=zeros(size(spect,3),1);
for i=1:size(spect,3)
	img=squeeze(spect(:,:,i));
	img2=squeeze(spect2(:,:,i));
	co=corrcoef(img(:),img2(:));
	r(i)=co(1,2);
end
figure();
plot(freqs(idxs),r); ylabel('Correlation'); xlabel('Frequency');
title('Correlation between cross-spectra per frequency');
fprintf(1,'Mean correlation between cross-spectra slices: %f\n', mean(r));

% Examine baseline drift

% Baseline drifts
XDrift = noiseModel.generate('nSamples',size(dat.X,1), 'multiplier', 500/1000);

[specNoise,freqs] = pwelch(Xnoise(:,1),[],[],[],dat.samplingFreq);
specDrift = pwelch(XDrift(:,1),[],[],[],dat.samplingFreq);
figure(); hold on;
plot(freqs,10*log10(specNoise),'r');
plot(freqs,10*log10(specDrift),'g');
title('Drift');
legend('Original','Drifted');


	

