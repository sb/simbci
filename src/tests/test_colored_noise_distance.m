
% Test colored noise derived from electrode distance

clear;
close all;

% Parameters of the generated data
samplingFreq = 200;
nSecs = 1*60;
nSamples = nSecs * samplingFreq;
physicalModel = core_head('filename',expand_path('sabre://models/leadfield_256elec_sigma15'));

% Make noise model based on electrode distance + generate
noiseModel = noisemodel_distance('physicalModel', physicalModel);
Xnoise = noiseModel.generate('samplingFreq',samplingFreq, 'nSamples',nSamples);

% Make a cross-spectral density model of the generated data
noiseModel1 = noisemodel_cross_spectral('source', Xnoise, 'samplingFreq', samplingFreq);	

% Compare
figure(); 
subplot(1,2,1); imagesc(noiseModel.model.crossSpectrum); title('Distance based cross spectrum');	
subplot(1,2,2); imagesc(abs(mean(noiseModel1.model.crossSpectrum,3))); title('Estim. from gen. data');

figure();
pwelch(Xnoise(:,1),[],[],[],samplingFreq);

if(0)
	% Tests drifts; unfortunately this method does not guarantee that the generated file would have
	% the same power as the original ... 
	% @fixme Test disabled until the generator is fixed
	XDrift = noiseModel.generate('nSamples',size(Xnoise,1), 'multiplier', 500/1000);

	[specNoise,freqs] = pwelch(Xnoise(:,1),[],[],[],samplingFreq);
	specDrift = pwelch(XDrift(:,1),[],[],[],samplingFreq);
	figure(); hold on;
	plot(freqs,10*log10(specNoise),'r');
	plot(freqs,10*log10(specDrift),'g');
	title('Drift');
	legend('Original','Drifted');
end




