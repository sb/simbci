
% Runs the noise generators related to the spectrally weighted noise from the BCI Competition IV review paper

clear;
close all;

% Use a real recording to estimate the spectral characteristics
freqLow = 0.5;
freqHigh = 60;

%%%% test _cross_spectral
dat=load('E:/jl/datasets/motor-imagery-rennes-jussi-gtec2008-20160104/motor-imagery-csp-4-online-[2016.01.04-15.18.47].mat');
dat.X=dat.samples; dat.samples=[];
dat.X = filter_bandpass(dat.X, dat.samplingFreq, freqLow, freqHigh, 4);
dat.id='motorimagery';
save('simbci_temp.mat','-struct','dat');
Xnoise = noise_spectrally_colored( size(dat.X), 'subType', 'dataDriven', 'sourceFile','simbci_temp.mat', 'dataset',dat);

%%%% test _spatial
physicalModel = core_head('filename',expand_path('sabre://models/leadfield_256elec_sigma15'));
Xnoise2 = noise_spectrally_colored( size(dat.X), 'subType', 'spatial', 'dataset',dat, 'sourceFile','simbci_temp.mat', 'physicalModel',physicalModel);

%%%% test _distance
Xnoise3 = noise_spectrally_colored( size(dat.X), 'subType', 'distance','dataset',dat,'physicalModel',physicalModel);

