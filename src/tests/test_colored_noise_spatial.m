%
% Tests colored noise derived from pink noise + covariance specification
%

clear;
close all;

% Use a real recording to estimate the spectral characteristics
freqLow = 0.5;
freqHigh = 60;

dat=load('E:/jl/datasets/motor-imagery-rennes-jussi-gtec2008-20160104/motor-imagery-csp-4-online-[2016.01.04-15.18.47].mat');
%dat=load('C:/jl/datasets/motor-imagery-rennes-jussi-gtec2008-20160104/motor-imagery-csp-4-online-[2016.01.04-15.23.42].mat');
%dat=load('C:/jl/datasets/motor-imagery-rennes-jussi-gtec2008-20160104/motor-imagery-csp-4-online-[2016.01.04-15.29.04].mat');
dat.X=dat.samples; dat.samples=[];
dat.X = filter_bandpass(dat.X, dat.samplingFreq, freqLow, freqHigh, 4);
dat.id='motorimagery';

% Background noise
noiseModel = noisemodel_spatial('source',dat.X);
Xnoise = noiseModel.generate('nSamples',size(dat.X,1));

figure();
subplot(1,2,1); imagesc(cov(dat.X)); title('Data covariance');
subplot(1,2,2); imagesc(cov(Xnoise)); title('Generated data covariance');

[specOrig,freqs] = pwelch(dat.X(:,1),[],[],[],dat.samplingFreq);
specGen = pwelch(Xnoise(:,1),[],[],[],dat.samplingFreq);
figure(); hold on;
plot(freqs,10*log10(specOrig),'r');
plot(freqs,10*log10(specGen),'g');
title('Channel 1 spectrum');
legend('Orig','Generated');

