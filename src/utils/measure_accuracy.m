
function accuracy = measure_accuracy(target, predicted, ~, ~, ~)
% Computes the ratio of correct predictions to all predictions. Result will be in range [0,1].

	accuracy = mean(target==predicted);

end

