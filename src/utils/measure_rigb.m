
function result = measure_rigb(target, predicted, pipelineParams, trainData, testData)
% RIGb error measure from N.J. Hill & al. "A general method for assessing brain-computer
% interface performance and its limitations", 2014.
%
% The measure returns 'bits per minute'. RIGb is estimated by dividing the information
% gain in bits per trial by the mean duration of a trial. Note that the result can be
% negative, indicating that the predictions were worse than achieved by a 'guess' baseline.
%
% To get an estimate of 'guess accuracy' this measure relies on, we assume a baseline classifier 
% that predicts the majority class in the train set. This is not the easiest baseline in a sense
% that in some cases we can have a classifier consistently doing worse: one that would predict 
% a uniform distribution of labels even if the true distribution was skewed, or a non-majority class.
%

	% Find out the events that indicate trials to classify
	p = inputParser;
	p.KeepUnmatched = true;
	p.CaseSensitive = true;
	p.PartialMatching = false;

	addParameter(p, 'classEvents',            {},      @iscell); % which events are we interested in

	p.parse(pipelineParams{:});
	
	classEvents = p.Results.classEvents;
				
	% find out the most frequent class in the train data
	classCountTrain = zeros(1,length(classEvents));	
	for i=1:length(classEvents)
		for j=1:length(trainData.events)
			if(strcmp(trainData.events(j).type,classEvents(i)))
				classCountTrain(i) = classCountTrain(i) + 1;
			end
		end	
	end
	[~,mostFrequentIdx] = max(classCountTrain);
	
	% find out the class counts and the trial durations in the dataset we're testing
	trialDurationsTest = zeros(1,length(classEvents));
	classCountTest = zeros(1,length(classEvents));		
	for i=1:length(classEvents)
		for j=1:length(testData.events)
			if(strcmp(testData.events(j).type,classEvents(i)))
				classCountTest(i) = classCountTest(i) + 1;
				trialDurationsTest(i) = trialDurationsTest(i) + testData.events(j).duration;
			end
		end	
	end
	numTrials = sum(classCountTest);
	
	% convert durations to seconds
	trialDurationsTest = trialDurationsTest / testData.samplingFreq;
	
	% using the notation of the paper
	P0    = classCountTest(mostFrequentIdx)/numTrials;  % accuracy of guessing the most common class in training set
	P     = mean(target==predicted);                    % observed accuracy of the predictions
	tMean = sum(trialDurationsTest)/numTrials;		    % average trial duration in seconds
		
	% Attempt to handle singularities in the measure by truncating to [eps,1-eps] range
	P0 = min(max(P0,eps),1-eps); P = min(max(P,eps),1-eps);
	
	% The actual measure
	result = sign(P-P0) ...
		*( P*log2( P/P0 ) + (1-P)*log2( (1-P)/(1-P0) ) ) ...
		/ tMean;
	result = 60*result; % convert to minutes
	
	if(isnan(result) || isinf(result))
		% if it still went over, fallback to 'no gain, no loss'. This shouldn't usually happen.
		result = 0;
	end

end

