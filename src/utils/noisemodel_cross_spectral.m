%
% A model that estimates cross spectral density tensor from a given dataset.
% The cross-spectral density tensor is a structure like D(f,c,d), where f 
% is frequency and c and d are channels. New data is sampled from a Gaussian
% distribution and then imprinted the spectral dependncy structure from the
% tensor. 
%
% This class can be used as part of a pipeline to replicate the BCI Competition IV
% artifical dataset (Tangermann & al. 2012, sections 4.2.1 and 4.2.2)
%
classdef noisemodel_cross_spectral

	properties
		model
	end
	
	methods

	function obj = noisemodel_cross_spectral(varargin)
	% Estimate cross spectral tensor of a given dataset. The spectrum can be later 
	% used to generate noise with the similar spectral characteristics.
	
		p = inputParser;
		p.KeepUnmatched = false;
		p.CaseSensitive = true;
		p.PartialMatching = false;

		addParameter(p, 'source',                 [],             @isnumeric); % signal matrix for datadriven
		addParameter(p, 'samplingFreq',           200,            @isnumeric); 
		addParameter(p, 'visualize',              false,          @islogical);
		addParameter(p, 'window',                 [],             @isnumeric); % to cpsd, [] = samplingFreq = 1 sec
		addParameter(p, 'bins',                   [],             @isnumeric); % to cpsd, [] = matlab default
		addParameter(p, 'overlap',                [],             @isnumeric); % to cpsd, [] = matlab default

		p.parse(varargin{:});
		params = p.Results;

		%%%%%%

		assert(~isempty(params.source), 'For data driven spectrum, source matrix is needed');

		% estimate the cross-spectrum model from real data in 'source'

		% Todo: Possibly a reasonable way to get a decent, smooth estimate would be to average the cross-spectrums
		% from several recordings. But this might lose the correlations dependending on changes in conditions.

		nChannels=size(params.source,2);

		%fprintf(1,'Estimating cross spectrum...');

		if(isempty(params.window))
			window = params.samplingFreq;
		else
			window = params.window;
		end

		bins = params.bins; 
		overlap = params.overlap;

		% Run the cpsd once to obtain the number of estimates
		[tmp,frequencies] = cpsd(params.source(:,1),params.source(:,1),window,overlap,bins,params.samplingFreq);
		nPowerEstimates = size(tmp,1);

		% fprintf(1,'Spectrum size: %d\n', nPowerEstimates);

		% @TODO this computation could probably be much more faster, likely now
		% the same fft transforms are done over and over again in cpsd. However, the
		% n(n-1)/2 nested loops [ i.e. p=cspd(i,j)] did not seem to be faster.
		crossSpectrum = zeros(nChannels,nChannels,nPowerEstimates);
		for i=1:nChannels
			xpower = cpsd(params.source(:,i),params.source,window,overlap,bins,params.samplingFreq);
			crossSpectrum(i,:,:) = xpower';
		end

		if(0)
			% slower
			crossSpectrum2 = zeros(nChannels,nChannels,nPowerEstimates);
			for i=1:nChannels
				for j=i:nChannels
					[xpower,frequencies] = cpsd(params.source(:,i),params.source(:,j),window,overlap,bins,params.samplingFreq);
					crossSpectrum2(i,j,:) = xpower;
					crossSpectrum2(j,i,:) = conj(xpower);	% symmetry
				end
				fprintf(1,'.');
			end
			fprintf('\n');
		end

		obj.model=[];
		obj.model.crossSpectrum = crossSpectrum;
		obj.model.frequencies = frequencies;
		obj.model.type='estimated';
		obj.model.mean = mean(params.source);
		obj.model.std = std(params.source);
		obj.model.cov = cov(params.source);
		obj.model.window = window;

		if(params.visualize)

			figure();
			imagesc(crossSpectrum(:,:,1));
			title('First slice of the estimated cross spectrum');
			xlabel('Electrode'); ylabel('Electrode');
		end	
	end

	function [noiseColored, interpolatedSpectrum, f] = generate(obj, varargin)
	% generate 'nSamples' new samples while trying to imprint the model dependency structure
	%
	% 'Multiplier' > 1 can be used to squeeze the spectrum towards the low frequencies
	% to generate slow 'baseline drifts'.
	%
	% Output f contains the summed eigenvalues per each frequency slice of the interpolated spectrum.
	%
		p = inputParser;
		p.KeepUnmatched = true;
		p.CaseSensitive = true;
		p.PartialMatching = false;

		addParameter(p, 'nSamples',               100,            @isnumeric);   % how many samples to generate
		addParameter(p, 'multiplier',             1.0,            @isnumeric);   
		addParameter(p, 'interpolation',          'linear',       @ischar);      % passed to interp1 in data driven noise
		addParameter(p, 'extrapolation',          10,             @isnumeric);   % how many frequency bins to extrapolate
		addParameter(p, 'visualize',              false,          @islogical);	
		addParameter(p, 'copyChannelMoments',     true,           @islogical);   % copy channel mean+var from source file?

		p.parse(varargin{:});
		params = p.Results;

		%%%

		assert(~isempty(obj.model), 'This method requires a spectral model');

		dim = size(obj.model.crossSpectrum,1);

		nSamples = params.nSamples;
		nSamplesModded = false;
		if rem(nSamples,2)
			nSamples=nSamples + 1;
			nSamplesModded = true;
		end

		% Due to FFT symmetry ...
		uniquePts = nSamples/2 + 1;

		% Generates Gaussian white noise, maps it to time-frequency space, modulating it to
		% have a specified spectral covariance structure which can be different for each frequency.
		% The followed frequency power law comes from the spectral model.
		%
		% TODO this computation for big, dense datasets is very very slow. Maybe
		% it could be made faster or at least have a smaller memory footprint by
		% doing the interpolation manually at each step in the uniquepts loop. Also,
		% instead of eigdecomposing each slice separately, maybe the orignal
		% stack could be decomposed and then the interpolation done for the eigs?

		% Generate Gaussian white noise and convert to the freq space
		noiseFFTColored = fft( randn(nSamples, dim) );

		% In this case we have a different spectral covariance for every freq.
		% Interpolate the spectrum stack to match the generated dataset size
		interpolatedSpectrum = zeros(dim,dim,uniquePts);
		sourceFrequencies = params.multiplier * obj.model.frequencies;
		targetFrequencies = linspace(0, obj.model.frequencies(end), uniquePts);

		if(params.extrapolation<=0)
			for i=1:dim
				for j=i:dim
					spectrum = squeeze(obj.model.crossSpectrum(i,j,:));
					xpowerInterp = interp1(sourceFrequencies, spectrum, targetFrequencies, params.interpolation,0);
					interpolatedSpectrum(i,j,:) = xpowerInterp;
					interpolatedSpectrum(j,i,:) = conj(xpowerInterp);
				end
			end
		else
			% duplicating code to avoid putting an if() inside the loop
			extrapolationIdxs = (length(sourceFrequencies)-params.extrapolation+1):length(sourceFrequencies);
			for i=1:dim
				for j=1:dim
					spectrum = squeeze(obj.model.crossSpectrum(i,j,:));
					xpowerInterp = interp1(sourceFrequencies, ...
						spectrum, targetFrequencies, params.interpolation, mean(spectrum(extrapolationIdxs)));
					interpolatedSpectrum(i,j,:) = xpowerInterp;
					interpolatedSpectrum(j,i,:) = conj(xpowerInterp);
				end
			end			
		end

		% fprintf(1,'Generating noise');

		% Filter the white noise to give it the desired spectral covariance structure

		% Due to fft symmetry we can do just the half and mirror the rest
		f=zeros(1,uniquePts);
		for i=1:uniquePts
	%		if(rem(i,5000)==0) fprintf(1, '.'); end


			freqCov = interpolatedSpectrum(:,:,i);
			[E,D] = eig(freqCov);
			noiseFFTColored(i,:) = ((E*sqrt(D))*noiseFFTColored(i,:)')';
			f(i)=sum(real(sqrt(diag(D))));
		end
	%	plot(f);
	%	fprintf(1,'\n');

		noiseFFTColored(uniquePts+1:nSamples,:) = real(noiseFFTColored(nSamples/2:-1:2,:)) -1i*imag(noiseFFTColored(nSamples/2:-1:2,:));

		noiseColored = real(ifft(noiseFFTColored));


		if(0)
			% force sources spatial covariance structure. Note that this may adversely affect the earlier spectral structuring we did.
			[E1,D1] = eig(cov(noiseColored));
			noiseColoredWhite = noiseColored*real(E1*sqrt(pinv(D1)));
			[E2,D2] = eig(obj.model.cov);
			noiseColored = (real(E2*sqrt(D2))*noiseColoredWhite')';
		end

		if(nSamplesModded)
			noiseColored = noiseColored(1:nSamples-1,:);
		end

		% set the channel means and variances from the original signal?	
		if(params.copyChannelMoments)
			% First scale to 0 mean, unit variance 	and then specify these params from the sample data
			noiseColored = normalize_to(noiseColored, obj.model.mean, obj.model.std);
		end
		
	end
	
	end % methods
	
end
	

