classdef noisemodel_distance
% This model makes an ad-hoc cross-spectrum based on electrode distances:
% the close electrodes have stronger dependency.
% The cross-spectrum will be the same for all frequencies.
%

	properties
		model
	end
	
	methods

	function obj = noisemodel_distance(varargin)
	% Construct the model.
		
		p = inputParser;
		p.KeepUnmatched = false;
		p.CaseSensitive = true;
		p.PartialMatching = false;

		addParameter(p, 'physicalModel',          [],             @ishead);
		addParameter(p, 'visualize',              false,          @islogical);

		p.parse(varargin{:});
		params = p.Results;

		%%%%%%

		assert(~isempty(params.physicalModel), 'physicalModel is needed by this method');
		nChannels = size(params.physicalModel.electrodePos,1);

		% Weight the spectrum so that channels which are close by are more correlated (gaussian weighted distance)
		crossSpectrum = zeros(nChannels, nChannels);
		for i=1:nChannels
			for j=i:nChannels
				d = norm(params.physicalModel.electrodePos(i,:) - params.physicalModel.electrodePos(j,:));
				crossSpectrum(i,j) = exp(-(d.^2));
				crossSpectrum(j,i) = crossSpectrum(i,j);
			end
		end

		obj.model=[];
		obj.model.crossSpectrum = crossSpectrum;
		obj.model.frequencies = 1;
		obj.model.type = 'distance';
		obj.model.mean = zeros(1, nChannels);
		obj.model.std = ones(1,nChannels);
		
		if(params.visualize)
			figure();
			imagesc(crossSpectrum);
			title('Distance-based cross spectrum');
			xlabel('Electrode'); ylabel('Electrode');
			% ...
		end

	
	end
	
	function noiseColored = generate(obj, varargin )
	% Sample new data from the model
		
		p = inputParser;
		p.KeepUnmatched = true;
		p.CaseSensitive = true;
		p.PartialMatching = false;

		addParameter(p, 'samplingFreq',           200,            @isnumeric);
		addParameter(p, 'exponent',               1.7,            @isnumeric);   % 'a' for 1/f^a type spectra
		addParameter(p, 'nSamples',               100,            @isnumeric);   % how many samples to generate
		addParameter(p, 'visualize',              false,          @islogical);	
		addParameter(p, 'copyChannelMoments',     true,           @islogical);   % copy channel mean+var from source file?

		p.parse(varargin{:});
		params = p.Results;

		%%%

		assert(~isempty(obj.model), 'This method requires a spectral model');
		assert(length(obj.model.frequencies)==1, 'Should have a flat spectral model');

		dim = size(obj.model.crossSpectrum,1);

		nSamples = params.nSamples;
		nSamplesModded = false;
		if rem(nSamples,2)
			nSamples=nSamples + 1;
			nSamplesModded = true;
		end

		% Due to FFT symmetry ...
		uniquePts = nSamples/2 + 1;

		% Generates Gaussian white noise, maps it to time-frequency space, modulating it to
		% have a specified 1/f type slope and spectral covariance structure, identical for every frequency.

		% Generate Gaussian white noise and convert to the freq space
		noiseFFTColored = fft( randn(nSamples, dim) );

		% in this case the cross spectrum is same for each freq, and we add an 1/freq power law here.
		% n.b. In total, this probably resembles generating pink noise and enforcing a spatial cov structure for it.
		[E,D] = eig(obj.model.crossSpectrum);
		%figure(1); imagesc(E); drawnow;
			
		targetFrequencies = linspace(0, params.samplingFreq/2, uniquePts);
		targetFrequencies(1) = targetFrequencies(2); % avoid div by zero
		
		% Filter the white noise to give it the desired covariance structure
		noiseFFTColored = noiseFFTColored*real(E*sqrt(D))';
		noiseFFTColored(1,:) = 0;	% zero DC
		
		weighting = 1./sqrt(targetFrequencies.^params.exponent);
		
		% from Euler's formula; but with this the spectrum becomes 'too clean'...
		%noiseFFTColored(1:uniquePts,:) = exp(1i*angle(noiseFFTColored(1:uniquePts,:))) ...
		%	.* repmat(weighting', [1 dim]);
		noiseFFTColored(1:uniquePts,:) = noiseFFTColored(1:uniquePts,:) .* repmat(weighting', [1 dim]);
		
		noiseFFTColored(uniquePts+1:nSamples,:) = real(noiseFFTColored(nSamples/2:-1:2,:)) -1i*imag(noiseFFTColored(nSamples/2:-1:2,:));
		
		noiseColored = real(ifft(noiseFFTColored));
		
		if(nSamplesModded)
			noiseColored = noiseColored(1:nSamples-1,:);
		end
		
		% set the channel means and variances from the original signal?	
		if(params.copyChannelMoments)
			% First scale to 0 mean, unit variance 	and then specify these params from the sample data
			noiseColored = normalize_to(noiseColored, obj.model.mean, obj.model.std);
		end
	
		if(params.visualize)
			figure();
			subplot(1,2,1); plot(log(weighting)); title('Log weighting');
			subplot(1,2,2); pwelch(noiseColored(:,1),[],[],[],params.samplingFreq); title('First chn psd');
		end
	end
	
	end % methods
	
end
	

