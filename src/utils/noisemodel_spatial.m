
classdef noisemodel_spatial
% This method creates pink noise with 1/f^a spectrum,
% while copying mean and covariance structure from a given input data
%

	properties
		model
	end
	
	methods

	function obj = noisemodel_spatial(varargin)
	% Estimates data mean and covariance	
		p = inputParser;
		p.KeepUnmatched = false;
		p.CaseSensitive = true;
		p.PartialMatching = false;

		addParameter(p, 'source',                 [],             @isnumeric); % signal matrix for datadriven
		addParameter(p, 'visualize',              false,          @islogical);

		p.parse(varargin{:});
		params = p.Results;

		%%%%%%

		assert(~isempty(params.source), 'For data driven spectrum, source matrix is needed');

		obj.model=[];
		obj.model.crossSpectrum = [];
		obj.model.frequencies = [];
		obj.model.type = 'spatial';
		obj.model.mean = mean(params.source);
		obj.model.std = std(params.source);
		obj.model.cov = cov(params.source);
		obj.model.window = [];

		if(params.visualize)
			figure();
			imagesc(obj.model.cov);
			title('Covariance structure');
			xlabel('Electrode'); ylabel('Electrode');
		end

	end
	
	function noiseColored = generate(obj, varargin )
	% generate 'nSamples' new samples having the same covariance structure as the given spectral model
	%	
		p = inputParser;
		p.KeepUnmatched = true;
		p.CaseSensitive = true;
		p.PartialMatching = false;

		addParameter(p, 'exponent',               1.7,            @isnumeric);   % for 1/f^a type spectra
		addParameter(p, 'nSamples',               100,            @isnumeric);   % how many samples to generate
		addParameter(p, 'visualize',              false,          @islogical);	
		addParameter(p, 'copyChannelMoments',     true,           @islogical);   % copy channel mean+var from source file?

		p.parse(varargin{:});
		params = p.Results;

		%%%

		assert(~isempty(obj.model), 'This method requires a spectral model');

		dim = size(obj.model.cov,1);
		nSamples = params.nSamples;

		% This creates pink 1/f noise which is then specified a spatial cov structure

		% Create pink noise
		noiseGen = dsp.ColoredNoise('InverseFrequencyPower',params.exponent,'NumChannels',dim,'SamplesPerFrame',nSamples);
		noisePink = noiseGen.step();

		% Transform the pink noise to be spatially white.
		co = cov(noisePink);
		[E,D] = eig(co);
		whiteningMatrix = inv (sqrt (D)) * E';
		whitenedNoise = (whiteningMatrix * noisePink')';

		% Transform it to have a preferred spatial covariance spectrum
		[ETarget,DTarget] = eig(obj.model.cov);
		dewhiteningMatrix = ETarget * sqrt (DTarget);
		noiseColored = (dewhiteningMatrix * whitenedNoise')';

		% set the channel means and variances from the original signal?	
		if(params.copyChannelMoments)
			% First scale to 0 mean, unit variance 	and then specify these params from the sample data
			noiseColored = normalize_to(noiseColored, obj.model.mean, obj.model.std);
		end	
	
	end
	
	end % methods
	
end
	

