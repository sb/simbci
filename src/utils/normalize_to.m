
function [data,me,sd] = normalize_to(data,targetMean,targetSd,alongDim)
% Transforms data columns or rows to have the target mean and unit variance

	if(nargin<4)
		alongDim = 1;
	end

	sd = std(data, [], alongDim);
	me = mean(data, alongDim);

	if(alongDim == 1)
		data = data - repmat( me, [size(data,1) 1]);
		data = data .* repmat( targetSd./sd, [size(data,1) 1]);	
		data = data + repmat( targetMean, [size(data,1) 1]);
	else
		data = data - repmat( me, [1 size(data,2)]);
		data = data .* repmat( targetSd./sd, [1 size(data,2)]);
		data = data + repmat( targetMean, [1 size(data,2)]);		
	end

end

