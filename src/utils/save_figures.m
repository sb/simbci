function save_figures( prefix, useExtFig )
%SAVE_FIGURES Dumps all currently open figures to pics/
%   Detailed explanation goes here

	if(nargin<1)
		prefix = 'debug';
	end
	if(nargin<2)
		useExtFig = false;
	end
	
	fprintf(1, 'Saving figs to pics/ ...');

	figHandles = get(0,'Children');
	if(length(figHandles)==0)
		fprintf(1, '\nError: No figures currently open\n');
		return;
	end
	
	% Delete old pics with the same prefix
	if(useExtFig)	
		pattern = expand_path(sprintf('sabre:pics/%s*.pdf', prefix));
	else
		pattern = expand_path(sprintf('sabre:pics/%s*.png', prefix));
	end
	delete(pattern);
	
	for i=1:length(figHandles)
			h = get(figHandles(i));
			if(useExtFig)
				fn = expand_path(sprintf('sabre:pics/%s%03d.pdf', prefix, h.Number));
				set(figHandles(i), 'Color', 'w') 				
				export_fig(h.Number,'-pdf',fn);
			else
				fn = expand_path(sprintf('sabre:pics/%s%03d.png', prefix, h.Number));	
				print(h.Number,'-dpng','-r300',fn);
			end
			fprintf(1,'.');
	end
	fprintf(1,' done\n');


end

