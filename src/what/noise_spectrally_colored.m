
function [noise,details,params] = noise_spectrally_colored( sizeof, varargin )
% Concatenate the different types of surface noise used in the BCI Comp IV review paper (4.2.1, 4.2.2)

	p = inputParser;
	p.KeepUnmatched = false;
	p.CaseSensitive = true;
	p.PartialMatching = false;

	addParameter(p, 'dataset',                [],                       @isstruct);
	addParameter(p, 'mask',                   [],                       @isnumeric); 
	addParameter(p, 'physicalModel',          @null,                    @ishead);
	addParameter(p, 'multiplier',             [1.0,300/1000,150/1000],  @isnumeric); % anything less than 1 is a drift
	addParameter(p, 'multiplierStrengths',    [1.0,0.5,0.3],            @isnumeric); % mixing coefficient of different drifts	
	addParameter(p, 'subType',                'distance',              @ischar);
	addParameter(p, 'sourceFile',             '/path/src.mat',          @ischar);
	addParameter(p, 'exponent',               1.7,                      @isnumeric);
	addParameter(p, 'visualize',              false,                    @islogical);

	p.parse(varargin{:});
	params = p.Results;

	%%%%%%%%%

	assert(length(params.multiplier)==length(params.multiplierStrengths), 'delays and delayStrengths must have the same length');
	
%	assert(~isempty(params.dataset));
%	assert(~isequal(params.physicalModel,@null));
%	assert(size(params.physicalModel.electrodePos,1)==sizeof(2), 'For this noise type, number of channels requested should match head model electrode count');
	
	samplingFreq = params.dataset.samplingFreq;
	physicalModel = params.physicalModel;

	% fprintf(1, 'Estimating cross spectrum...\n');
	if(strcmp(params.subType, 'spatial'))
		% Artificial spatial correlation. Easiest to understand.
		% this generation appears to be more stable in its cov
		% characteristics from run to another, if the drifts are
		% disabled.
		dat = load(expand_path(params.sourceFile));			
		noiseModel = noisemodel_spatial('source',dat.X, 'visualize', params.visualize);
	elseif(strcmp(params.subType, 'distance'))
		% Artificial spectral correlation, same for all frequencies.		
		noiseModel = noisemodel_distance('physicalModel', physicalModel, 'visualize', params.visualize);
	elseif(strcmp(params.subType, 'dataDriven'))
		% Data-driven spectral correlation trying to mimic the statistics of the file.
		% This is the one actually in the paper. Assumed the file follows the conventions of the platform
		dat = load(expand_path(params.sourceFile));

		assert(dat.samplingFreq == samplingFreq);

		noiseModel = noisemodel_cross_spectral( ...
			'source',dat.X, 'samplingFreq',samplingFreq, 'visualize',params.visualize);
	elseif(strcmp(params.subType, 'prebuilt'))
		% Load a prebuilt model from disk
		noiseModel = load(expand_path(params.sourceFile));
	else
		assert(false, 'Unknown bcicomp4 subtype');
	end

	if(~strcmp(params.subType,'dataDriven'))
		% the other methods don't have a working multiplier implementation
		multipliersToUse = 1;
	else
		multipliersToUse = length(params.multiplier);
	end
	
	details = [];
	details.noiseModel = noiseModel;
		
	% Using 3 multipliers and corresponding weights in the loop can implement background noise, bci comp iv 4.2.1, drift 1, bci comp iv 4.2.2, drift 2, bci comp iv 4.2.2	
	% Note that components from sections 4.2.3, 4.2.4 and 4.2.5 would be generated in the volume and not here.
	for i=1:multipliersToUse
		% @todo rework this; now we pass params to models that do not need or use them. 
		% move to constructor. n.b. multiplier is a problem there.
		tmp = params.multiplierStrengths(i) .* noiseModel.generate( ...
			'samplingFreq', samplingFreq, 'exponent', params.exponent, ...
			'nSamples', sizeof(1), 'multiplier', params.multiplier(i));
		if(i==1)
			noise = tmp;
		else
			noise = noise + tmp;
		end
	end
		
	if(~isempty(params.mask))
		noise = noise .* repmat(params.mask, [1 sizeof(2)]);
	end

	if(params.visualize)
		numChn = min(9,size(noise,2));
		subPlots = ceil(sqrt(numChn));
		figure();
		yMax = max(max(noise(:,1:numChn)));
		yMin = min(min(noise(:,1:numChn)));

		for i=1:numChn
			subplot( subPlots, subPlots, i);
			plot( (0:sizeof(1)-1)/samplingFreq, noise(:,i));
			xlabel('Time');ylabel('Amplitude');
			ylim([yMin yMax]);
			title(sprintf('bcicomp4 noise: chn %d', i));
		end
	end
end

